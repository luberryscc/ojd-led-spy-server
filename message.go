package main

import "time"

type Message struct {
	Connected bool      `json:"connected,omitempty"`
	Id        string    `json:"id,omitempty"`
	Buttons   []Button  `json:"buttons"`
	Axes      []string  `json:"axes"`
	Timestamp time.Time `json:"timestamp"`
}
type Button struct {
	Pressed bool    `json:"pressed"`
	Value   float64 `json:"value"`
}
