# OJD LED Spy Server

This is a server meant to broadcast the data from the [LED Spy Project](https://gitlab.com/Luberry/smashboxledspy) to Open Joystick Display
over the network. 

## Commands

`--list-ports` (`-l`) - lists the serial ports available
`--led-spy-port` (`-p`) - sets the serial port you wish to listen on
`--host` (`-h`) - sets the host to listen on, you shouldn't need to touch this.

## Examples
* Windows 
  ```powershell
  .\ojd-led-spy-server-0.0.1-amd64.exe -l
  Found port: COM8

  .\ojd-led-spy-server-0.0.1-amd64.exe -p COM8
  ```

* Mac
  ```sh
  ./ojd-led-spy-server-0.0.1-mac -l
  Found port: /dev/tty.Bluetooth-Incoming-Port
  Found port: /dev/tty.MALS
  Found port: /dev/tty.SOC
  Found port: /dev/tty.usbserial-146140

  ./ojd-led-spy-server-0.0.1-mac -p /dev/tty.usbserial-146140
  ```

* linux
  ```sh
  ./ojd-led-spy-server-0.0.1-linux-amd64 -l
  Found port: /dev/ttyUSB0

  ./ojd-led-spy-server-0.0.1-linux-amd64 /dev/ttyUSB0
  ```

