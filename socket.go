package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"net"
	"strconv"
	"strings"
)

type JSONSocket struct {
	conn net.Conn
}

func SendMessage(conn net.Conn, msg interface{}) error {
	data, err := formatMessage(msg)
	if err != nil {
		return err
	}
	if _, err = conn.Write(data); err != nil {
		return err
	}
	return nil
}

func ReadMessage(conn net.Conn, msg interface{}) error {
	rd := bufio.NewReader(conn)

	lr, err := rd.ReadString('#')
	if err != nil {
		return err
	}
	l, err := strconv.Atoi(strings.TrimSuffix(lr, "#"))
	if err != nil {
		return err
	}
	buf := make([]byte, l)
	_, err = rd.Read(buf)
	if err != nil {
		return err
	}

	return json.Unmarshal(buf, msg)
}

func formatMessage(msg interface{}) ([]byte, error) {
	rawMsg, err := json.Marshal(msg)
	if err != nil {
		return nil, err
	}
	var b bytes.Buffer
	b.WriteString(strconv.Itoa(len(rawMsg)))
	b.WriteRune('#')
	b.Write(rawMsg)
	return b.Bytes(), nil
}
