module ojd-led-spy-server

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/pflag v1.0.5
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	go.bug.st/serial v1.1.3
)
