package main

import (
	"bufio"
	"fmt"
	"net"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	flag "github.com/spf13/pflag"
	"go.bug.st/serial"
)

var (
	comport     string
	host        string
	list        bool
	serialport  serial.Port
	closeFunc   func() error
	smux        sync.Mutex
	currentData []byte
)

const (
	buttonCount = 27
	id          = "Hit Box Smash Box (LED Spy)"
	readCount   = buttonCount + 1*2
)

func init() {
	flag.StringVarP(&comport, "led-spy-port", "p", "", "COM port of the LED Spy Board")
	flag.StringVarP(&host, "host", "h", ":56709", "address for the server to listen on. `host:port` if host is not specified it will listen on 0.0.0.0")
	flag.BoolVarP(&list, "list-ports", "l", false, "list com ports")
	flag.Parse()
}

func main() {
	if list {
		ports, err := serial.GetPortsList()
		if err != nil {
			logrus.WithError(err).Fatal()
		}
		if len(ports) == 0 {
			fmt.Println("No serial ports found!")
		} else {
			for _, port := range ports {
				fmt.Printf("Found port: %v\n", port)
			}
		}
		return
	}
	if host == "" {
		logrus.Fatal("`host` cannot be empty")
	}
	if comport == "" {
		logrus.Fatal("`ledspy-port` cannot be empty")
	}

	ln, err := net.Listen("tcp", host)
	if err != nil {
		logrus.WithError(err).Fatal()
	}

	go func() {
		if err := initSerial(); err != nil {
			serialError(err)
		}
	}()
	defer closeFunc()

	for {
		conn, err := ln.Accept()
		if err != nil {
			logrus.WithError(err).Error()
		}
		go handleServerConnection(conn)

	}
}
func handleServerConnection(c net.Conn) {
	remoteAddr := c.RemoteAddr().String()
	logrus.Println("Client connected from", remoteAddr)

	for {
		msg := map[string]bool{}
		if err := ReadMessage(c, &msg); err != nil {
			logrus.WithError(err).Error()
			break
		}

		m, err := read()
		if err != nil {
			go serialError(err)
		}
		SendMessage(c, m)
	}

	logrus.Println("Client at", remoteAddr, "disconnected")
}
func serialError(err error) {
	logrus.WithError(err).Error()
	if err := initSerial(); err != nil {
		serialError(err)
	}
}

func read() (*Message, error) {
	m := &Message{
		Id:        id,
		Timestamp: time.Now(),
		Connected: true,
		Axes:      []string{},
		Buttons:   []Button{},
	}
	for _, v := range currentData {
		btn := Button{}
		if v == 49 {
			btn.Pressed = true
			btn.Value = 1
		}
		m.Buttons = append(m.Buttons, btn)
	}
	return m, nil
}

func initSerial() (err error) {
	logrus.Printf("Connecting to %s\n", comport)
	smux.Lock()
	defer smux.Unlock()
	if serialport != nil {
		serialport.Close()
		serialport = nil
	}
	serialport, err = serial.Open(comport, &serial.Mode{BaudRate: 115200})
	if err != nil {
		return
	}
	closeFunc = serialport.Close
	logrus.Printf("Connected to %s\n", comport)
	serialport.ResetInputBuffer()
	scanner := bufio.NewScanner(serialport)
	fmt.Println("scanning")
	for scanner.Scan() {
		currentData = scanner.Bytes()
		//fmt.Println(currentData)
	}
	return nil
}
